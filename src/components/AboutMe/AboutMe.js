import React from 'react';
import { Container, Button } from 'react-bootstrap';

import CV from '../../images/ResumeOscar.pdf';
import "./AboutMe.scss";

export default function AboutMe() {
    return (
        <Container className = "about-me">
            <p> 
                Soy una persona comprometida y orientada a resultados, disfruto
                tomando nuevos desafíos. Me encanta trabajar en equipos transversales 
                y alcanzar las metas planteadas. Me gusta aprender nuevas tecnologías
                asi como la comunicación y la gestión de proyectos.
  
            </p>
            <hr/>
            <a href={CV} target="_blankc" >
                <Button primary = "true" >Descargar CV</Button>
            </a>
            
        </Container>
    )
}
