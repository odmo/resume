import React from 'react';
import { Container, Row, Col, Image } from "react-bootstrap";


import profileImage from "../../images/oscar.jpg";
import Social from './Social';
import "./Profile.scss";

const data = [
    {
        title: "Edad:",
        info : "31 años"
    },
    {
        title: "email",
        info: "oscar.david.moreno@gmail.com"
    },
    {
        title: "telefono",
        info: "+52 (663) 444-54-36"
    }
];

export default function Profile() {
    return (
        <div className = "profile">
            <div className = "wallpaper"/>
            <div className = "dark"/>
            <Container className = "box" >
                <Row className = "info">
                    <Col xs = {12} md = {4}>
                        <Image  src={profileImage} fluid />                
                    </Col>
                    <Col xs = {12} md = {8} className = "info__data" >
                        <span>
                            !Hola soy!
                        </span>
                        <p>Moreno Ortega Oscar David</p>
                        <p>Ingeniero en computacion</p>
                        <hr/>
                        <div className="more__info" >
                            {
                                data.map( (item, index) => (
                                    <div key = {index} className = "item">
                                        <p>{item.title}</p>
                                        <p>{item.info}</p>
                                    </div>
                                ) ) 
                            }
                        </div>
                    </Col>
                </Row>
                <Social />
            </Container>
        </div>
    )
}
