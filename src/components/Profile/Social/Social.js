import React from 'react';

import "./Social.scss";
import { Linkedin } from 'react-bootstrap-icons'

const socialMedia = [
    {
        icon : <Linkedin/>,
        link : "https://www.linkedin.com/in/oscar-david-moreno-ortega-34781a101/" 
    }
];


export default function Social() {
    return (
        <div className = "social">
            {
                socialMedia.map( (social, index ) => (
                    <a 
                        key = {index}  
                        href = {social.link} 
                        target = "_blank" 
                        rel= "noreferrer noopener"
                    >
                        {social.icon}
                    </a>
                ))
            }
        </div>
    )
}
