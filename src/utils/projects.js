
import ibsm from "../images/projects/ibsm.jpg"
import jacome from "../images/projects/jacome.png"
import excel from "../images/projects/excel.png"
import angular from "../images/projects/angular.jpg"
import irk from "../images/projects/irk.png"
import systems from "../images/projects/systems.jpg"
import gg from "../images/projects/gglobal.jpg"
import atisa from "../images/projects/atisa.png"
import vware from "../images/projects/vware.png"
import delta from "../images/projects/DeltaLogo.png"
import foxconn from "../images/projects/foxconn.png"
export default [
    {
        title : "Delta Solutions",
        description :"Lead Software Architect ",
        url: "/delta",
        image : delta,
        address : `Tijuana, Baja California, México`,
        start : `Abril 2023`,
        end: null,
        position : `Liderar y desarrollar proyectos de desarrollo de software. 
            Colaborar con otros ingenieros de software, responsable de determinar, 
            planificar y delegar tareas en todos los proyectos de desarrollo de software.`,
        web: null
    },
    {
        title : "Foxconn",
        description :"Business Analyst Engineer",
        url: "/foxconn",
        image : foxconn,
        address : `Laguna Mainar 5520-Sección C, Parque Industrial, El Lago, 22217 Tijuana, B.C`,
        start : `Septiembre 2022`,
        end: `Abril 2023`,
        position : `Analizar y evaluar los procesos y sistemas de la organización para 
                    identificar oportunidades de mejora y optimización. Alinear los 
                    procesos de negocio con las metas y objetivos estratégicos de 
                    la empresa.`,
        web: "https://foxconnbc.com/"
    },
    {
        title : "Vware Solutions",
        description :"Especialista en ingeniería de software",
        url: "/vware",
        image : vware,
        address : `Tijuana, Baja California, México`,
        start : `Diciembre 2021`,
        end: `Septiembre 2022`,
        position : `Diseño, desarrollo, implementación y mantenimiento de software. 
            Desarrollo de aplicaciones web, sistemas de bases de datos y sistemas de 
            control de versiones.
            Trabajar con los usuarios finales y otros miembros 
            del equipo para entender y documentar los requisitos del software.`,
        web: 'https://www.vwaresol.com/'
    },
    {
        title : "Atisa Group",
        description :"Especialista en ingeniería de software",
        url: "/atisa",
        image : atisa,
        address : `Blvd. Acapulco 14700, Parque Industrial Pacifico. CP. 22644 Tijuana, Baja California, México.`,
        start : `Marzo 2021`,
        end: `Diciembre 2021`,
        position : `Liderar y desarrollar proyectos de desarrollo de software. 
            Colaborar con otros ingenieros de software, responsable de determinar, 
            planificar y delegar tareas en todos los proyectos de desarrollo de software.
            Trabajar con los usuarios finales y otros miembros 
            del equipo para entender y documentar los requisitos del software.`,
        web: 'https://www.atisagroup.com/'
    },
    {
        title : "G-Global",
        description :"Especialista en ingeniería de software",
        url: "/gglobal",
        image : gg,
        address : `Blvd. de las Bellas Artes 17541-int 3, Garita de Otay, 22430 Tijuana, B.C.`,
        start : `Noviembre 2019`,
        end: `Marzo 2021`,
        position : `Desarrollar proyectos de desarrollo de software. 
            Colaborar con otros ingenieros de software.
            Trabajar con los usuarios finales y otros miembros 
            del equipo para entender y documentar los requisitos del software.`,
        web: 'https://g-global.com/'
    },
    {
        title : "Systems Communications",
        description : "Software Engineer",
        url: "/systemscommunications",
        image : systems,
        address : `José López Portillo Oriente N.º 350, Colonia Nueva Tijuana, Tijuana, Baja California, CP. 22435`,
        start : `Julio 2018`,
        end: `Noviembre 2019`,
        position : `Desarrollar proyectos de desarrollo de software. 
            Colaborar con otros ingenieros de software.
            Trabajar con los usuarios finales y otros miembros 
            del equipo para entender y documentar los requisitos del software.`,
        web: 'https://www.systemscomm.net/'
    },
    {
        title : "Instituto Ramiro Kolbe",
        description : "Profesor de tecnologías",
        url: "/irk",
        image : irk,
        address : `Camino Vecinal 11998, Pórticos Santa Fe Tijuana B.C. 22666, México.`,
        start : `Agosto 2017`,
        end: `Julio 2018`,
        position : `Experto en el diseño de planes de estudio y materiales educativos para mejorar 
            el rendimiento de los estudiantes en matemáticas. Habilidad para enseñar a estudiantes 
            de diferentes niveles de habilidad y utilizar tecnología educativa para mejorar la 
            enseñanza y el aprendizaje.`,
        web: 'http://irk.mx/'
    },
    {
        title : " SEI Torre Angular ",
        description : "Ingeniero en telecomunicaciones e Infraestructura",
        url: "/angular",
        image : angular,
        address : `Av. Ensenada esquina, Madero Sur, 22046 Tijuana, B.C.`,
        start : `Marzo 2017`,
        end: `Septiembre 2017`,
        position : `Diseño, implementación y mantenimiento de sistemas de telecomunicaciones 
            y redes de datos, así como en la infraestructura física necesaria para soportar 
            estas tecnologías. Manejo de redes de telecomunicaciones, sistemas de transmisión 
            y recepción de datos, sistemas de conmutación, sistemas de cableado estructurado, 
            sistemas de seguridad y vigilancia.`,
        web: null
    },
    {
        title : "Excel distribuidora",
        description : "Ingeniero en telecomunicaciones e Infraestructura",
        url: "/excel",
        image : excel,
        address : `Fco. Eusebio Kino Norte #105. Frac. Garita de Otay CP 22430 Tijuana, B.C.`,
        start : `Agosto 2016`,
        end: `Diciembre 2016`,
        position : `Diseño, implementación y mantenimiento de sistemas de telecomunicaciones 
            y redes de datos, así como en la infraestructura física necesaria para soportar 
            estas tecnologías. Manejo de redes de telecomunicaciones, sistemas de transmisión 
            y recepción de datos, sistemas de conmutación, sistemas de cableado estructurado, 
            sistemas de seguridad y vigilancia.`,
        web: 'https://excel.mx/'
    },
    {
        title : "Instituto Bilingüe Santillana del Mar Rosarito",
        description : "Profesor de tecnologías y robótica",
        url: "/ibsm",
        image : ibsm,
        address : `Leonardo Bravo, 10 Col. independencia Playas de Rosarito, B.c.`,
        start : `Diciembre 2015`,
        end: `Julio 2016`,
        position : `Experto en el diseño de planes de estudio y materiales educativos para mejorar 
            el rendimiento de los estudiantes en matemáticas. Habilidad para enseñar a estudiantes 
            de diferentes niveles de habilidad y utilizar tecnología educativa para mejorar la 
            enseñanza y el aprendizaje.`,
        web: 'https://www.santillanadelmar.edu.mx/'
    },
    {
        title : "Instituto Jacome Rosarito",
        description : "Profesor de matemáticas y álgebra",
        url: "/jacome",
        image : jacome,
        address : `Calle Raúl Sánchez Díaz #3, Colonia Echeverría 22703 Rosarito, Península de Baja California, México`,
        start : `Agosto 2014`,
        end: `Enero 2015`,
        position : `Experto en el diseño de planes de estudio y materiales educativos para mejorar 
            el rendimiento de los estudiantes en matemáticas. Habilidad para enseñar a estudiantes 
            de diferentes niveles de habilidad y utilizar tecnología educativa para mejorar la 
            enseñanza y el aprendizaje.`,
        web: 'https://www.facebook.com/InstitutoJacomeRosarito/'
    }

];