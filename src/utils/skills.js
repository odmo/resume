export const frontendSkills = [
    {type: "Análisis", level: 90},
    {type: "Comunicación.", level: 90},
    {type: "Team_work.", level: 80},
    {type: "Instruir.", level: 80},
];

export const frontendSkillsColor = {
    bar : "#3498db",
    title : {
        text : "#fff",
        background: "#2980b9"
    }
}

export const backendSkills = [
    {type: "Procesos", level: 80},
    {type: "Técnicos", level: 90},
    {type: "Resolución.", level: 70},
    {type: "Gestión.", level: 80},
];

export const backendSkillsColor = {
    bar : "#00bd3f",
    title : {
        text : "#fff",
        background: "#009331"
    }
}

export const soSkills = [
    {type: "OS", level: 70},
    {type: "Unix", level: 80},
];

export const soSkillsColor = {
    bar : "#f09c00",
    title : {
        text : "#fff",
        background: "#b46900"
    }
}

export const techSkills = [
    {type: "O.S.", level: 100},
    {type: "Desarrollo", level: 90},
    {type: "Infraestructura", level: 90},
];

export const techSkillsColor = {
    bar : "#f09c00",
    title : {
        text : "#fff",
        background: "#b46900"
    }
}

export const lastSkills = [
    
];

export const lastkillsColor = {
    bar : "#f09c00",
    title : {
        text : "#fff",
        background: "#b46900"
    }
}