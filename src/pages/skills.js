import React from "react";
import  {Container} from 'react-bootstrap';

import "./skills.scss";
import BasicLayout from "../Layouts/BasicLayout";
import ListSkills from "../components/ListSkills";

import { 
  frontendSkills, 
  frontendSkillsColor, 
  backendSkills, 
  backendSkillsColor, 
  techSkills, 
  techSkillsColor,
  lastSkills,
  lastkillsColor
} from '../utils/skills';

export default function Index(){
  return (
    <BasicLayout menuColor = "#000">
      <Container>
        <div className="skills__block">
          <h2>Soft Skills</h2>
          <ListSkills skills = {frontendSkills} colors = { frontendSkillsColor }/>
        </div>
        <div className="skills__block">
          <h2>Hard Skills</h2>
          <ListSkills skills = {backendSkills} colors = { backendSkillsColor }/>
        </div>
        <div className="skills__block">
          <h2>Otras Habilidades</h2>
          <ListSkills skills = {techSkills} colors = { techSkillsColor }/>
        </div>
        <div className="skills__block">
          <h2></h2>
          <ListSkills skills = {lastSkills} colors = { lastkillsColor }/>
        </div>
      </Container>
    </BasicLayout>
  );
}
  

