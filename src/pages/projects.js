import React from "react";
import { Container, Row, Col, Card, Button, Accordion } from "react-bootstrap";

import BasicLayout from "../Layouts/BasicLayout";
import './projects.scss';
import projects from "../utils/projects";

export default function Projects (props){

  return (
    <BasicLayout menuColor = "#000">
      <Container className="projects">
        <h1> Experiencia </h1>
        
        <Row>

          { 
            projects.map(
              ( project, index ) => (
                <Col key={index} xs={12} sm= {4} className = "project" >
                  <Card>
                    <div 
                      className = "image" 
                      style = {{ 
                        backgroundImage : `url("${project.image}")`,
                      }} 
                    />
                    <Card.Body>
                      <Card.Title>
                        {project.title}
                      </Card.Title>
                      <Card.Text>
                        {project.description}
                      </Card.Text>
                      <Accordion alwaysOpen>
                        <Accordion.Item eventKey="0">
                          <Accordion.Header>Periodo</Accordion.Header>
                          <Accordion.Body>
                            <p>Inicio : {project.start}</p>
                            {project.end && <p>Final : {project.end}</p>}
                            <p>Direccion : {project.address}</p>
                          </Accordion.Body>
                        </Accordion.Item>
                        <Accordion.Item eventKey="1">
                          <Accordion.Header>Descripción</Accordion.Header>
                          <Accordion.Body>
                           {project.position}
                          </Accordion.Body>
                        </Accordion.Item>
                      </Accordion>
                      { 
                        project.web && <a href={project.web} target="_blank" rel="noreferrer"> 
                          <Button variant = "primary"> Web </Button>
                        </a>
                      }
                    </Card.Body>
                  </Card>
                </Col>
              )
            )
          }
          
        </Row>
      </Container>
    </BasicLayout>
  );
}
